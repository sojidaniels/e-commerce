# Ecomm

## About The Project

This project is a micorservice for an ecommerce platform.


### Built With

* [Javascript/Nodejs](https://nodejs.org/)
* [Python/FastAPI](https://fastapi.tiangolo.com/)
* [Go/Gin](https://github.com/gin-gonic/gin)
* [Ruby/Rails](https://rubyonrails.org/)
* [Docker](https://www.docker.com/)


## Getting Started

Ensure [Docker](https://docs.docker.com/get-docker/) is installed on your machine


### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/sojidaniels/e-commerce
   ```
2. Create a .env file in the root directory of this project
3. start application
   ```sh
   ./scripts/start.sh
   ```


## Usage

Download the postman collection in the postman directory to use the apis for this microservices


## Roadmap

- [ ] Build a user interface
