require('dotenv').config();

const environment = process.env.NODE_ENV;

const Services = {
    carting: 'http://carting-app-service:3030',
    catalog: 'http://catalog-app-service:3000',
    wallet: 'http://wallet-app-service:8000',
    purchase: 'http://purchase-app-service:4000',
};

if (environment === 'development') {
    Services.carting = 'http://carting:3030';
    Services.catalog = 'http://catalog:3000';
    Services.wallet = 'http://wallet:8000';
    Services.purchase = 'http://purchase:4000';
}




module.exports = { Services };
